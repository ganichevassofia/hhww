package Задача2;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("З/п в час (в $)(не меньше 8$): ");
        double zp = in.nextDouble();
        if (zp < 8){
            System.out.println("Работник не может получать меньше, чем 8$ в час ");
            return;
        }
        System.out.println("Число рабочих часов в неделе ( меньше 60): ");
        byte hours = in.nextByte();
        if (hours>60) {
            System.out.println("Работник не может работать больше 60 часов в неделю");
            return;
        }
        double zarp = 0;
        for (byte i=1; i<=hours;i++) {
            if (i<=40) {
                zarp +=zp;
            } else {
                zarp+=1.5*zp;
            }
        }
        System.out.print("З/п работника в неделю составляет: ");
        System.out.printf("%.2f", zarp);
        System.out.print(" $");
	// write your code here
    }
}
